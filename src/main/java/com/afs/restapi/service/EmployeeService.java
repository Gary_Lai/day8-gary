package com.afs.restapi.service;

import com.afs.restapi.exception.EmployeeAgeException;
import com.afs.restapi.exception.EmployeeSalaryNotMatchAgeException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {


    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }


    public Employee getEmployeeById(int id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findEmployeesByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee insertEmployee(Employee employee) {
        if (employee.getAge() < 18 || employee.getAge() > 65) {
            throw new EmployeeAgeException();
        }
        if (employee.getAge() > 30 && employee.getSalary() < 20000) {
            throw new EmployeeSalaryNotMatchAgeException();
        }
        employee.setStatus(true);
        return employeeRepository.insert(employee);
    }

    public Employee updateEmployee(int id, Employee employee) {
        return employeeRepository.update(id, employee);
    }

    public void deleteEmployee(int id) {
        Employee employee = employeeRepository.findById(id);
        if (employee != null) {
            employee.setStatus(false);
        }
    }
}
