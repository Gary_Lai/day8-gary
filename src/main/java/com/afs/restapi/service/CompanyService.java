package com.afs.restapi.service;

import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    private CompanyRepository companyRepository;
    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }


    public List<Company> getAllCompanies() {
        return companyRepository.getCompanies();
    }

    public Company getCompanyById(int id) {
        return companyRepository.getCompanyById(id);
    }

    public List<Employee> getEmployeesByCompanyId(int companyId) {
        return companyRepository.getEmployeesByCompanyId(companyId);
    }

    public List<Company> getCompanyByPage(Integer page, Integer size) {
        return companyRepository.getCompanyByPage(page, size);
    }

    public void addCompany(Company company) {
        companyRepository.addCompany(company);
    }

    public Company updateCompanyById(int id, Company company) {
        return companyRepository.updateCompanyById(id, company);
    }

    public void removeById(int id) {
        companyRepository.removeById(id);
    }

}
