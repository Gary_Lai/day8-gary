package com.afs.restapi.exception;

public class EmployeeNotFoundException extends RuntimeException {
    public EmployeeNotFoundException() {
        super("employee id not found");
    }
}
