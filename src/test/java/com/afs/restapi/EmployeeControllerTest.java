package com.afs.restapi;

import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeControllerTest {

    @Autowired
    MockMvc mockMvc;
    ObjectMapper mapper = new ObjectMapper();
    @Autowired
    EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp() {
        employeeRepository.clearAll();
    }

    @Test
    void should_get_all_employees_when_perform_get_given_employees() throws Exception {
        //given
        Employee newEmployee = buildEmployeeSusan();
        employeeRepository.insert(newEmployee);

        //when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/employees"))
                .andReturn();

        //then
        MockHttpServletResponse response = mvcResult.getResponse();
        assertEquals(200, response.getStatus());
        List<Employee> employees = mapper.readValue(response.getContentAsString(), new TypeReference<List<Employee>>() {
        });
        assertEquals(1, employees.size());
        Employee employeeGet = employees.get(0);
        assertEquals(1, employeeGet.getId());
        assertEquals("Susan", employeeGet.getName());
        assertEquals(22, employeeGet.getAge());
        assertEquals("Female", employeeGet.getGender());
        assertEquals(10000, employeeGet.getSalary());
    }

    private static Employee buildEmployeeSusan() {
        Employee newEmployee = new Employee(1, "Susan", 22, "Female", 10000);
        newEmployee.setStatus(true);
        return newEmployee;
    }

    @Test
    void should_get_all_employees_when_perform_get_given_employees_use_matchers() throws Exception {
        //given
        Employee newEmployee = buildEmployeeSusan();
        employeeRepository.insert(newEmployee);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("Susan"))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value(22))
                .andExpect(jsonPath("$[0].gender").isString())
                .andExpect(jsonPath("$[0].gender").value("Female"))
                .andExpect(jsonPath("$[0].salary").isNumber())
                .andExpect(jsonPath("$[0].salary").value(10000));
    }

    @Test
    void should_return_employee_susan_with_id_1_when_perform_get_by_id_given_employees_in_repo() throws Exception {
        //given
        Employee employee1 = buildEmployeeSusan();
        Employee employee2 = buildEmployeeLisi();
        employeeRepository.insert(employee1);
        employeeRepository.insert(employee2);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/employees/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value("Susan"))
                .andExpect(jsonPath("$.age").isNumber())
                .andExpect(jsonPath("$.age").value(22))
                .andExpect(jsonPath("$.gender").isString())
                .andExpect(jsonPath("$.gender").value("Female"))
                .andExpect(jsonPath("$.salary").isNumber())
                .andExpect(jsonPath("$.salary").value(10000));
    }

    private static Employee buildEmployeeLisi() {
        Employee employee2 = new Employee(2, "Lisi", 30, "Man", 5000);
        return employee2;
    }

    @Test
    void should_return_No_FOUND_when_perform_get_by_id_given_employees_in_db_and_id_not_in() throws Exception {
        //given
        Employee employee1 = new Employee(1, "Susan", 22, "Female", 10000);
        Employee employee2 = new Employee(2, "Lisi", 30, "Man", 5000);
        Employee employee3 = new Employee(3, "WangWu", 30, "Man", 6000);
        employeeRepository.insert(employee1);
        employeeRepository.insert(employee2);
        employeeRepository.insert(employee3);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/employees/6"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void should_return_employees_by_params_when_perform_get_by_params_given_employees_in_repo() throws Exception {
        //given
        Employee employee1 = buildEmployeeSusan();
        Employee employee2 = buildEmployeeLisi();
        Employee employee3 = buildEmployeeWangwu();
        employeeRepository.insert(employee1);
        employeeRepository.insert(employee2);
        employeeRepository.insert(employee3);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/employees?gender=Man"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(2))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("Lisi"))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value(30))
                .andExpect(jsonPath("$[0].gender").isString())
                .andExpect(jsonPath("$[0].gender").value("Man"))
                .andExpect(jsonPath("$[0].salary").isNumber())
                .andExpect(jsonPath("$[0].salary").value(5000))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(3))
                .andExpect(jsonPath("$[1].name").isString())
                .andExpect(jsonPath("$[1].name").value("WangWu"))
                .andExpect(jsonPath("$[1].age").isNumber())
                .andExpect(jsonPath("$[1].age").value(30))
                .andExpect(jsonPath("$[1].gender").isString())
                .andExpect(jsonPath("$[1].gender").value("Man"))
                .andExpect(jsonPath("$[1].salary").isNumber())
                .andExpect(jsonPath("$[1].salary").value(6000));
    }

    private static Employee buildEmployeeWangwu() {
        Employee employee3 = new Employee(3, "WangWu", 30, "Man", 6000);
        return employee3;
    }


    @Test
    void should_return_one_page_employees_when_perform_get_by_page_given_employees_in_repo() throws Exception {

        //given
        Employee employee1 = buildEmployeeSusan();
        Employee employee2 = buildEmployeeLisi();
        Employee employee3 = buildEmployeeWangwu();
        employeeRepository.insert(employee1);
        employeeRepository.insert(employee2);
        employeeRepository.insert(employee3);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/employees?pageNumber=1&pageSize=2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("Susan"))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value(22))
                .andExpect(jsonPath("$[0].gender").isString())
                .andExpect(jsonPath("$[0].gender").value("Female"))
                .andExpect(jsonPath("$[0].salary").isNumber())
                .andExpect(jsonPath("$[0].salary").value(10000))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].name").isString())
                .andExpect(jsonPath("$[1].name").value("Lisi"))
                .andExpect(jsonPath("$[1].age").isNumber())
                .andExpect(jsonPath("$[1].age").value(30))
                .andExpect(jsonPath("$[1].gender").isString())
                .andExpect(jsonPath("$[1].gender").value("Man"))
                .andExpect(jsonPath("$[1].salary").isNumber())
                .andExpect(jsonPath("$[1].salary").value(5000));
    }

    @Test
    void should_return_employee_save_with_id_when_perform_post_given_a_employee() throws Exception {
        //given
        Employee employeeSusan = buildEmployeeSusan();
        String susanJson = mapper.writeValueAsString(employeeSusan);

        //when
        mockMvc.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(susanJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value("Susan"))
                .andExpect(jsonPath("$.age").isNumber())
                .andExpect(jsonPath("$.age").value(22))
                .andExpect(jsonPath("$.gender").isString())
                .andExpect(jsonPath("$.gender").value("Female"))
                .andExpect(jsonPath("$.salary").isNumber())
                .andExpect(jsonPath("$.salary").value(10000));
        Employee employeeSaved = employeeRepository.findById(1);
        assertEquals(employeeSusan.getId(), employeeSaved.getId());
        assertEquals(employeeSusan.getAge(), employeeSaved.getAge());
        assertEquals(employeeSusan.getGender(), employeeSaved.getGender());
        assertEquals(employeeSusan.getName(), employeeSaved.getName());
        assertEquals(employeeSusan.getSalary(), employeeSaved.getSalary());
    }

    @Test
    void should_update_employee_in_repo_when_perform_put_by_id_given_employee_in_repo_and_update_info() throws Exception {

        //given
        Employee employeeSusan = buildEmployeeSusan();
        employeeRepository.insert(employeeSusan);
        Employee toBeUpdateSusan = buildEmployeeSusan();
        toBeUpdateSusan.setSalary(20000);
        String susanJson = mapper.writeValueAsString(toBeUpdateSusan);

        //when
        mockMvc.perform(MockMvcRequestBuilders.put("/employees/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(susanJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value("Susan"))
                .andExpect(jsonPath("$.age").isNumber())
                .andExpect(jsonPath("$.age").value(22))
                .andExpect(jsonPath("$.gender").isString())
                .andExpect(jsonPath("$.gender").value("Female"))
                .andExpect(jsonPath("$.salary").isNumber())
                .andExpect(jsonPath("$.salary").value(20000));
        Employee suSanInRepo = employeeRepository.findById(1);
        assertEquals(toBeUpdateSusan.getSalary(), suSanInRepo.getSalary());
    }

    @Test
    void should_del_employee_in_repo_when_perform_del_by_id_given_employees() throws Exception {
        //given
        Employee employee = buildEmployeeSusan();
        employeeRepository.insert(employee);

        //when
        mockMvc.perform(MockMvcRequestBuilders.delete("/employees/{id}", 1))
                .andExpect(status().isNoContent());

        //then
        assertThrows(EmployeeNotFoundException.class, () -> employeeRepository.findById(1));
    }
}