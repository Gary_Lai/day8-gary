package com.afs.restapi;


import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {


    @Autowired
    MockMvc mockMvc;
    ObjectMapper mapper = new ObjectMapper();
    @Autowired
    CompanyRepository companyRepository;

    @BeforeEach
    void setUp() {
        companyRepository.clearAll();
    }

    @Test
    void should_get_all_companies_when_perform_get_given_companies() throws Exception {
        // given
        Employee employee = buildEmployeeSusan();
        Company csCompany = new Company(1, "CS", List.of(employee));
        companyRepository.addCompany(csCompany);

        // when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andReturn();

        //then
        MockHttpServletResponse response = mvcResult.getResponse();
        assertEquals(200, response.getStatus());
        List<Company> companies = mapper.readValue(response.getContentAsString(), new TypeReference<List<Company>>() {
        });
        assertEquals(1, companies.size());
        Company companyFirst = companies.get(0);
        assertEquals(1, companyFirst.getId());
        assertEquals(csCompany.getCompanyName(), companyFirst.getCompanyName());
        Employee employeeFirst = companyFirst.getEmployees().get(0);
        assertEquals(employeeFirst.getName(), employee.getName());
    }

    @Test
    void should_get_a_company_when_perform_get_given_companies_and_id() throws Exception {
        // given
        Employee employee = buildEmployeeSusan();
        int companyId = 1;
        Company csCompany = new Company(companyId, "CS", List.of(employee));
        companyRepository.addCompany(csCompany);

        // when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                        .get("/companies/{companyId}", companyId))
                .andReturn();

        //then
        MockHttpServletResponse response = mvcResult.getResponse();
        assertEquals(200, response.getStatus());
        Company companyResp = mapper.readValue(response.getContentAsString(), new TypeReference<Company>() {
        });
        assertEquals(companyId, companyResp.getId());
        assertEquals("CS", companyResp.getCompanyName());
        Employee employeeFirst = companyResp.getEmployees().get(0);
        assertEquals(employeeFirst.getName(), employee.getName());
    }

    @Test
    void should_get_employees_in_company_when_perform_get_given_companies_and_company_id() throws Exception {
        // given
        Employee employee = buildEmployeeSusan();
        int companyId = 1;
        Company csCompany = new Company(companyId, "CS", List.of(employee));
        companyRepository.addCompany(csCompany);

        // when then
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/companies/{companyId}/employees", companyId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("Susan"))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value(22))
                .andExpect(jsonPath("$[0].gender").isString())
                .andExpect(jsonPath("$[0].gender").value("Female"))
                .andExpect(jsonPath("$[0].salary").isNumber())
                .andExpect(jsonPath("$[0].salary").value(10000));
    }

    @Test
    void should_get_companies_when_perform_get_given_companies_and_page_size() throws Exception {
        // given
        Employee employee = buildEmployeeSusan();
        int companyId = 2;
        Company ooclCompany = new Company(1, "OOCL", null);
        Company csCompany = new Company(companyId, "CS", List.of(employee));
        companyRepository.addCompany(ooclCompany);
        companyRepository.addCompany(csCompany);

        // when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                        .get("/companies")
                        .param("pageIndex", String.valueOf(2))
                        .param("pageSize", String.valueOf(1)))
                .andReturn();

        //then
        MockHttpServletResponse response = mvcResult.getResponse();
        assertEquals(200, response.getStatus());
        List<Company> companies = mapper.readValue(response.getContentAsString(), new TypeReference<List<Company>>() {
        });
        assertEquals(1, companies.size());
        Company companyFirst = companies.get(0);
        assertEquals(companyId, companyFirst.getId());
        assertEquals(csCompany.getCompanyName(), companyFirst.getCompanyName());
        Employee employeeFirst = companyFirst.getEmployees().get(0);
        assertEquals(employeeFirst.getName(), employee.getName());
    }

    @Test
    void should_no_return_when_perform_post_given_company() throws Exception {
        // given
        Employee employee = buildEmployeeSusan();
        int companyId = 1;
        Company csCompany = new Company(companyId, "CS", List.of(employee));

        String requestBody = mapper.writeValueAsString(csCompany);

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .post("/companies")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody));

        //then
        Company company = companyRepository.getCompanyById(1);
        assertEquals(csCompany.getId(), company.getId());
        assertEquals(csCompany.getCompanyName(), company.getCompanyName());
    }

    @Test
    void should_return_company_when_perform_put_given_company_and_company_id() throws Exception {
        // given
        Employee employee = buildEmployeeSusan();
        int companyId = 1;
        Company csCompany = new Company(companyId, "CS", List.of(employee));
        companyRepository.addCompany(csCompany);
        Company updateCompany = new Company(companyId, "OOCL", List.of(employee));
        String requestBody = mapper.writeValueAsString(updateCompany);

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .put("/companies/{companyId}", companyId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody));

        //then
        Company afterUpdatedCompany = companyRepository.getCompanyById(1);
        assertEquals(updateCompany.getId(), afterUpdatedCompany.getId());
        assertEquals(updateCompany.getCompanyName(), afterUpdatedCompany.getCompanyName());
    }

    @Test
    void should_no_return_when_perform_delete_given_company_and_company_id() throws Exception {
        // given
        Employee employee = buildEmployeeSusan();
        int companyId = 1;
        Company csCompany = new Company(companyId, "CS", List.of(employee));
        companyRepository.addCompany(csCompany);

        // when
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/companies/{companyId}", companyId));

        //then
        assertThrows(CompanyNotFoundException.class, () -> companyRepository.getCompanyById(1));
    }

    @Test
    void should_return_No_FOUND_when_perform_get_by_id_given_companies_in_db_and_id_not_in() throws Exception {
        //given
        Employee employee = buildEmployeeSusan();
        int companyId = 1;
        Company csCompany = new Company(companyId, "CS", List.of(employee));
        companyRepository.addCompany(csCompany);
        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/6"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    private static Employee buildEmployeeSusan() {
        Employee newEmployee = new Employee(1, "Susan", 22, "Female", 10000);
        newEmployee.setStatus(true);
        return newEmployee;
    }

}
