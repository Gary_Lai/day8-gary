package com.afs.restapi.service;

import com.afs.restapi.exception.EmployeeAgeException;
import com.afs.restapi.exception.EmployeeSalaryNotMatchAgeException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class EmployeeServiceTest {

    private EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
    private EmployeeService employeeService = new EmployeeService(employeeRepository);


    @Test
    void should_throw_exception_when_add_given_employee_15_and_67() {
        // given
        Employee employeeYoung = new Employee(1, "zhangsan", 15, "Male", 1000);
        Employee employeeOld = new Employee(2, "lisi", 67, "Male", 100000);

        // when then
        assertThrows(EmployeeAgeException.class, () -> employeeService.insertEmployee(employeeYoung));
        assertThrows(EmployeeAgeException.class, () -> employeeService.insertEmployee(employeeOld));

    }

    @Test
    void should_throw_exception_and_no_call_when_add_given_employee_15_and_67() {
        // given
        Employee employeeYoung = new Employee(1, "zhangsan", 15, "Male", 1000);
        Employee employeeOld = new Employee(2, "lisi", 67, "Male", 100000);

        // when then
        assertThrows(EmployeeAgeException.class, () -> employeeService.insertEmployee(employeeYoung));
        assertThrows(EmployeeAgeException.class, () -> employeeService.insertEmployee(employeeOld));

        Mockito.verify(employeeRepository, times(0)).insert(any());
    }

    @Test
    void should_throw_exception_when_add_given_employee_age31_and_salary1000() {
        // given
        Employee employee = new Employee(1, "zhangsan", 31, "Male", 1000);

        // when then
        assertThrows(EmployeeSalaryNotMatchAgeException.class, () -> employeeService.insertEmployee(employee));

    }

    @Test
    void should_return_employee_when_add_given_employee_age31_and_salary30000() {
        // given
        Employee employee = new Employee(1, "zhangsan", 31, "Male", 30000);
        when(employeeRepository.insert(any())).thenReturn(employee);

        // when then
        assertNotNull(employeeService.insertEmployee(employee));

    }

    @Test
    void should_return_employee_with_status_true_when_insert_employee_given_a_employee_match_rule() {
        // given
        Employee employee = new Employee(1, "zhaosi", 38, "Male", 35000);
        Employee employeeToReturn = new Employee(1, "zhaosi", 38, "Male", 35000);
        employeeToReturn.setStatus(true);
        when(employeeRepository.insert(any())).thenReturn(employeeToReturn);

        // when
        Employee employeeSaved = employeeService.insertEmployee(employee);

        // then
        assertTrue(employeeSaved.getStatus());
        verify(employeeRepository).insert(argThat(employeeToSave -> {
            assertTrue(employeeToSave.getStatus());
            return true;
        }));
    }

    @Test
    void should_set_status_to_false_with_status_true_when_delete_employee_given_a_employee_match_rule() {
        // given
        Employee employee = new Employee(1, "zhaosi", 38, "Male", 35000);
        employee.setStatus(true);
        when(employeeRepository.findById(employee.getId())).thenReturn(employee);

        // when
        employeeService.deleteEmployee(employee.getId());

        // then
        assertFalse(employee.getStatus());

    }

    @Test
    void should_throw_exception_when_update_employee_given_a_employee_already_left() {
        // given
        Employee employee = new Employee(1, "zhaosi", 38, "Male", 35000);
        when(employeeRepository.update(employee.getId(), employee)).thenThrow(new EmployeeNotFoundException());

        // when then
        assertThrows(EmployeeNotFoundException.class, () -> employeeService.updateEmployee(employee.getId(), employee));

    }
}